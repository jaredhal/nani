var config = {
  type: Phaser.AUTO,
  width: window.screen.width - 5,
  height: window.screen.height - 20,
  physics: {
    default: "arcade",
    arcade: {
      gravity: {
        y: 0,
        x: 0,
      },
      debug: false,
    },
  },
  scene: {
    preload: preload,
    create: create,
    update: update,
  },
};

// Globals ###########################################################################

// Game Objects
var SPRITE_WAIFU;
var SPRITE_CAULDRON;
var SPRITE_JRDS_FACE;
var SPRITE_GROUP_PROJECTILES;
var PLAY_FIELD_BACKGROUND;

// Game Vars
var VAR_PLAYER_SPEED = 900;
var VAR_BEAN_SPEED = 300;
var VAR_JRD_VELOCITY = 300;
var VAR_SOUNDS_MUTED = false;

// Score & Health
var BEAN_SCORE = 0;
var WAIFU_HEALTH = 10;
var DISPLAYED_HEALTH = WAIFU_HEALTH;
var BEANS_TO_WIN = 20;

// UX
var HEALTH_BAR;
var BEGIN_MENU;

// Controls
var KEY_INPUT;

// Canvas Locations
var SCREEN_CENTER;
var PLAYER_RAILS = {
  right: null,
  left: null,
};

// Timings
var LAST_BEAN_SHOT;
var BEAN_INTERVAL;

// Sounds
var SOUND_NANI;
var SOUND_BEAN_SHOT_1;
var SOUND_BEAN_SHOT_2;
var SOUND_UWU;
var SOUND_CHIKI_BRIKI;
var SOUND_POP;
var SOUND_GAME_LOSE;
var SOUND_GAME_WIN;
var MUSIC_BEGIN_MENU;
var MUSIC_GAME_RUN;

// GAME INSTANCE
var GAME_STATE_CONSTS = {
  RUN: "run",
  PAUSE: "pause",
  DISPLAY_BEGIN_MENU: "beginMenu",
  OVER_LOSE: "overLose",
  OVER_WON: "overWon",
};
var GAME = new Phaser.Game(config);
var GAME_STATE = GAME_STATE_CONSTS.DISPLAY_BEGIN_MENU;

// ######################################################################################

// Functions: Utils

function playSound(sound) {
  if (!VAR_SOUNDS_MUTED) {
    sound.play();
  }
}

function playMusic(music, shouldPlay) {
  if (!VAR_SOUNDS_MUTED && shouldPlay && !music.isPlaying) {
    music.play();
  } else if (!shouldPlay) {
    music.stop();
  }
}

function setGameState(state) {
  GAME_STATE = state;
}

function drawHealthBar(scene, start_x, start_y, health) {
  var shape = new Phaser.Geom.Rectangle(start_x, start_y, 50 * health, 50);
  HEALTH_BAR = scene.add.graphics({
    fillStyle: {
      color: 0xff0000,
    },
  });
  HEALTH_BAR.fillRectShape(shape);
}

function genRandInt(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

function logger(domain, msg) {
  console.log(`[${domain}] ${msg}`);
}

function createBean() {
  return class Bean extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
      super(scene, x, y, "bean");
      this.speed = Phaser.Math.GetSpeed(VAR_BEAN_SPEED, 1);
      this.direction = [1, -1][Math.floor(2 * Math.random())];
    }

    fire(x, y) {
      this.setPosition(x, y);
      this.setActive(true);
      this.setVisible(true);
    }

    update(time, delta) {
      this.x += this.speed * this.direction * delta;

      if (this.x >= config.width || this.x < 0) {
        this.setActive(false);
        this.setVisible(false);
      }
    }
  };
}

function togglePlayField(display) {
  playMusic(MUSIC_GAME_RUN, display);

  SPRITE_JRDS_FACE.setVisible(display);
  PLAY_FIELD_BACKGROUND.setVisible(display);
  SPRITE_WAIFU.setVisible(display);
  SPRITE_CAULDRON.setVisible(display);
  HEALTH_BAR.setVisible(display);

  SPRITE_JRDS_FACE.setActive(display);
  PLAY_FIELD_BACKGROUND.setActive(display);
  SPRITE_WAIFU.setActive(display);
  SPRITE_CAULDRON.setActive(display);
  HEALTH_BAR.setActive(display);
}

function toggleBeginMenu(display) {
  playMusic(MUSIC_BEGIN_MENU, display);
  BEGIN_MENU.setActive(display);
  BEGIN_MENU.setVisible(display);
}

// Functions: Colliders
function collider_waifu(waifu, projectiles) {
  var isPlayerRight = waifu.x === PLAYER_RAILS.right;
  var sadTexture = isPlayerRight ? "playerSadRight" : "playerSadLeft";
  var returnTexture = isPlayerRight ? "playerRight" : "player";
  WAIFU_HEALTH -= 1;
  playSound(SOUND_UWU);
  logger("COLLIDER_WAIFU", `waifu health: ${WAIFU_HEALTH}`);
  projectiles.destroy();
  waifu.setTexture(sadTexture);
  if (WAIFU_HEALTH <= 0) {
    logger("GAME_STATE", "lose");
    setGameState(GAME_STATE_CONSTS.OVER_LOSE);
    playSound(SOUND_GAME_LOSE);
  }
  setTimeout(function () {
    waifu.setTexture(returnTexture);
  }, 500);
}

function collider_cauldron(cauldron, projectiles) {
  BEAN_SCORE += 1;
  playSound(SOUND_POP);
  logger("COLLIDER_CAULDRON", `beans caught: ${BEAN_SCORE}`);
  projectiles.destroy();
  if (BEAN_SCORE % 5 === 0) {
    playSound(SOUND_CHIKI_BRIKI);
  }
  if (BEAN_SCORE >= BEANS_TO_WIN) {
    logger("GAME_STATE", "win");
    setGameState(GAME_STATE_CONSTS.OVER_LOSE);
    playSound(SOUND_GAME_WIN);
  }
}

// Functions: Game
function preload() {
  // Load Images
  this.load.image("bg", "assets/bg.jpg");
  this.load.image("player", "assets/player.png");
  this.load.image("playerRight", "assets/playerRight.png");
  this.load.image("face", "assets/jrd.png");
  this.load.image("face_open", "assets/jrd_open.png");
  this.load.image("bean", "assets/bean.png");
  this.load.image("pot", "assets/pot.png");
  this.load.image("beginMenu", "assets/beginMenu.png");
  this.load.image("playerSadRight", "assets/playerSadRight.png");
  this.load.image("playerSadLeft", "assets/playerSadLeft.png");

  // Load Audio
  this.load.audio("naniSound", "assets/nani.mp3");
  this.load.audio("beanShotSound1", "assets/bean1.mp3");
  this.load.audio("beanShotSound2", "assets/bean2.mp3");
  this.load.audio("uwuSound", "assets/uwu.mp3");
  this.load.audio("chikiSound", "assets/chikiBriki.mp3");
  this.load.audio("popSound", "assets/pop.mp3");
  this.load.audio("loseSound", "assets/waimu.mp3");
  this.load.audio("winSound", "assets/winSound.mp3");
  this.load.audio("beginMenuMusic", "assets/beginMenuMusic.mp3");
  this.load.audio("gameRunMusic", "assets/gameRunMusic.mp3");
}

function create() {
  BEGIN_MENU = this.add.image(config.width / 2, config.height / 2, "beginMenu");
  BEGIN_MENU.setScale(2);

  PLAY_FIELD_BACKGROUND = this.add.image(
    config.width / 2,
    config.height / 2,
    "bg"
  );

  SOUND_BEAN_SHOT_1 = this.sound.add("beanShotSound1", { volume: 7 });
  SOUND_BEAN_SHOT_2 = this.sound.add("beanShotSound2", { volume: 7 });
  SOUND_NANI = this.sound.add("naniSound", { volume: 7 });
  SOUND_UWU = this.sound.add("uwuSound", { volume: 7 });
  SOUND_CHIKI_BRIKI = this.sound.add("chikiSound", { volume: 7 });
  SOUND_POP = this.sound.add("popSound", { volume: 7 });
  SOUND_GAME_LOSE = this.sound.add("loseSound", { volume: 7 });
  SOUND_GAME_WIN = this.sound.add("winSound", { volume: 7 });
  MUSIC_BEGIN_MENU = this.sound.add("beginMenuMusic", { loop: true });
  MUSIC_GAME_RUN = this.sound.add("gameRunMusic", { loop: true, volume: 0.7 });

  PLAYER_RAILS.left = 130;
  PLAYER_RAILS.right = config.width - 130;
  SCREEN_CENTER = config.height / 2;

  SPRITE_WAIFU = this.physics.add.sprite(
    PLAYER_RAILS.left,
    SCREEN_CENTER,
    "player"
  );
  SPRITE_WAIFU.setCollideWorldBounds(true);

  SPRITE_CAULDRON = this.physics.add.sprite(
    PLAYER_RAILS.right,
    SCREEN_CENTER,
    "pot"
  );
  SPRITE_CAULDRON.setCollideWorldBounds(true);
  SPRITE_CAULDRON.setScale(0.2);

  SPRITE_JRDS_FACE = this.physics.add.sprite(
    config.width / 2,
    config.height / 2,
    "face"
  );
  SPRITE_JRDS_FACE.setScale(2);
  SPRITE_JRDS_FACE.setCollideWorldBounds(true);

  KEY_INPUT = this.input.keyboard.createCursorKeys();
  BEAN_INTERVAL = genRandInt(500, 2000);

  SPRITE_GROUP_PROJECTILES = this.physics.add.group({
    classType: createBean(),
    maxSize: 30,
    runChildUpdate: true,
  });

  this.physics.add.collider(
    SPRITE_WAIFU,
    SPRITE_GROUP_PROJECTILES,
    collider_waifu
  );
  this.physics.add.collider(
    SPRITE_CAULDRON,
    SPRITE_GROUP_PROJECTILES,
    collider_cauldron
  );
  drawHealthBar(this, 10, 10, DISPLAYED_HEALTH);
}

function update() {
  if (GAME_STATE === GAME_STATE_CONSTS.DISPLAY_BEGIN_MENU) {
    //   Begin Menu
    toggleBeginMenu(true);
    togglePlayField(false);
    if (Phaser.Input.Keyboard.JustDown(KEY_INPUT.space)) {
      toggleBeginMenu(false);
      togglePlayField(true);
      setGameState(GAME_STATE_CONSTS.RUN);
    }
  } else if (GAME_STATE === GAME_STATE_CONSTS.OVER_LOSE) {
    //   Game Lost
    togglePlayField(false);
  } else if (GAME_STATE === GAME_STATE_CONSTS.OVER_WON) {
    //   Game Won
    togglePlayField(false);
  } else if (GAME_STATE === GAME_STATE_CONSTS.RUN) {
    //   Game Running
    if (SPRITE_JRDS_FACE.body.velocity.y === 0) {
      VAR_JRD_VELOCITY *= -1;
      SPRITE_JRDS_FACE.body.velocity.y = VAR_JRD_VELOCITY;
    }

    if (KEY_INPUT.up.isDown) {
      SPRITE_WAIFU.setVelocity(0, VAR_PLAYER_SPEED * -1);
      SPRITE_CAULDRON.setVelocity(0, VAR_PLAYER_SPEED * -1);
    } else if (KEY_INPUT.down.isDown) {
      SPRITE_WAIFU.setVelocity(0, VAR_PLAYER_SPEED);
      SPRITE_CAULDRON.setVelocity(0, VAR_PLAYER_SPEED);
    } else {
      SPRITE_WAIFU.setVelocity(0);
      SPRITE_CAULDRON.setVelocity(0);
    }

    if (Phaser.Input.Keyboard.JustDown(KEY_INPUT.space)) {
      playSound(SOUND_NANI);
      if (SPRITE_WAIFU.x == PLAYER_RAILS.left) {
        SPRITE_WAIFU.setTexture("playerRight");
        SPRITE_WAIFU.x = PLAYER_RAILS.right;
        SPRITE_CAULDRON.x = PLAYER_RAILS.left;
      } else {
        SPRITE_WAIFU.setTexture("player");
        SPRITE_WAIFU.x = PLAYER_RAILS.left;
        SPRITE_CAULDRON.x = PLAYER_RAILS.right;
      }
    }

    if (!LAST_BEAN_SHOT || new Date() - LAST_BEAN_SHOT > BEAN_INTERVAL) {
      var bean = SPRITE_GROUP_PROJECTILES.get();
      bean.setScale(0.09);

      if (bean) {
        SPRITE_JRDS_FACE.setTexture("face_open");
        setTimeout(function () {
          SPRITE_JRDS_FACE.setTexture("face");
        }, 500);
        bean.fire(SPRITE_JRDS_FACE.x, SPRITE_JRDS_FACE.y + 100);
      }
      LAST_BEAN_SHOT = new Date();
      BEAN_INTERVAL = genRandInt(500, 2000);
      playSound(
        [SOUND_BEAN_SHOT_2, SOUND_BEAN_SHOT_1][Math.floor(2 * Math.random())]
      );
    }

    if (WAIFU_HEALTH < DISPLAYED_HEALTH) {
      HEALTH_BAR.destroy();
      DISPLAYED_HEALTH = WAIFU_HEALTH;
      drawHealthBar(this, 10, 10, DISPLAYED_HEALTH);
    }
  }
}
